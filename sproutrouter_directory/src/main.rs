#![feature(proc_macro_hygiene)]
#![feature(decl_macro)]
#![feature(ip)]

#[macro_use]
extern crate rocket;

use sproutrouter_lib as lib;
use std::convert::TryFrom;
use std::net::{Ipv4Addr, Ipv6Addr};
use std::str::FromStr;
use std::sync::RwLock;

use rocket::State;
use rocket_contrib::json::Json;
use sproutrouter_lib::dict_node::Node;

#[macro_use]
extern crate serde_derive;

#[derive(Serialize, Clone)]
struct NodeList {
    nodes: Vec<Node>,
}

#[get("/")]
fn list_clients(node_list: State<RwLock<NodeList>>) -> Json<Vec<Node>> {
    Json(
        node_list
            .read()
            .expect("invalid nodelist rwlock")
            .nodes
            .clone(),
    )
}

use rocket::response::status::{Accepted, BadRequest};

#[put("/", data = "<new_node>")]
fn add_client(
    new_node: Json<Node>,
    node_list: State<RwLock<NodeList>>,
) -> Result<Accepted<()>, BadRequest<&'static str>> {
    let new_node = new_node.0;

    // Check IP addresses
    let contains_invalid = new_node
        .ip
        .iter()
        .filter(|ip_opt| {
            let str_ref = ip_opt.as_str();

            let ipv4_valid = match Ipv4Addr::from_str(str_ref) {
                Ok(addr) => addr.is_global() || addr.is_private(),
                Err(_) => false,
            };

            let ipv6_valid = match Ipv6Addr::from_str(str_ref) {
                Ok(addr) => {
                    addr.is_global()
                        || addr.is_unicast_link_local()
                        || addr.is_unicast_link_local_strict()
                }
                Err(_) => false,
            };

            !(ipv4_valid || ipv6_valid)
        })
        .next()
        .is_some();

    // Check public key
    let invalid_pubkey = lib::crypto::PublicKey::try_from(new_node.pub_key.as_str()).is_err();

    if contains_invalid {
        Err(BadRequest::<&'static str>(Some("invalid IP")))
    } else if invalid_pubkey {
        Err(BadRequest::<&'static str>(Some("invalid pubkey")))
    } else {
        let mut list = node_list.write().expect("invalid nodelist rwlock");
        list.nodes.push(new_node);
        Ok(Accepted::<()>(None))
    }
}

fn main() {
    rocket::ignite()
        .manage(RwLock::new(NodeList { nodes: Vec::new() }))
        .mount("/", routes![list_clients, add_client])
        .launch();
}
