#![feature(proc_macro_hygiene, decl_macro)]
#![feature(ip)]

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate derive_deref;
#[macro_use]
extern crate anyhow;
#[macro_use]
extern crate serde_derive;

use directory::{Directory, SafeNode};
use lib::crypto::KeyPair;
use prelude::*;

use std::convert::{TryFrom, TryInto};
use std::sync::{Arc, RwLock};
use std::thread::{self, JoinHandle};

use rocket::{config::Config, config::Environment};

mod client;
mod config;
mod directory;
mod node;
mod prelude;

fn register_at_dictionary_server(
    configuration: &config::Configuration,
    public_key: &str,
) -> Result<SafeNode, anyhow::Error> {
    // Take all non-loopback IPs
    let my_ips: Vec<String> = pnet::datalink::interfaces()
        .into_iter()
        .flat_map(|item| {
            item.ips
                .into_iter()
                .map(|ip| ip.ip())
                .filter(|ip| !ip.is_loopback())
                .map(|item| item.to_string())
        })
        .collect();

    // Create and send data to dictionary server
    let node = lib::dict_node::Node {
        ip: my_ips,
        pub_key: public_key.trim().into(),
    };

    let client = reqwest::blocking::Client::new();
    let res = client
        .put(format!("http://{}:8000", configuration.directory_server).as_str())
        .json(&node)
        .send()
        .context("Could not contact directory server")?;

    if res.status() != reqwest::StatusCode::ACCEPTED {
        anyhow::bail!(
            "directory server gave error: `{}`",
            res.text().context("could not extract error message")?
        );
    } else {
        SafeNode::try_from(node)
    }
}

fn launch_rocket(directory: Arc<RwLock<Directory>>) {
    rocket::custom(
        Config::build(Environment::Production)
            .port(CLIENT_PORT)
            .expect("Failed to use hardcoded Rocket config."),
    )
    .manage(directory)
    .mount("/", routes![client::request_url])
    .launch();
}

fn setup_node_functionality(
    directory: Arc<RwLock<Directory>>,
    own_main_keypair: KeyPair,
) -> (JoinHandle<()>, Arc<RwLock<bool>>) {
    let keep_running = Arc::new(RwLock::new(true));

    let keep_running_a = keep_running.clone();
    (
        thread::spawn(move || {
            node::start_listening_for_requests_as_node(keep_running_a, directory, own_main_keypair)
        }),
        keep_running,
    )
}

fn start_polling_directory_server(
    configuration: &config::Configuration,
) -> (
    std::thread::JoinHandle<()>,
    Arc<RwLock<bool>>,
    Arc<RwLock<Directory>>,
) {
    let keep_running = Arc::new(RwLock::new(true));
    let directory = Arc::new(RwLock::new(Directory::default()));

    let keep_running_a = keep_running.clone();
    let directory_a = directory.clone();
    let directory_server_url = configuration.directory_server.clone();

    let join_handle = std::thread::spawn(move || {
        directory::contact_directory_server(directory_server_url, directory_a, keep_running_a)
    });

    (join_handle, keep_running, directory)
}

fn main() -> Result<()> {
    let configuration =
        config::Configuration::from_flags().context("Failed to load configuration")?;

    // Create keypair
    let own_main_keypair = lib::crypto::KeyPair::gen().context("failed to generate keypair")?;
    let public_key: String = (&own_main_keypair.public_key()).try_into()?;

    // Launch directory server refresher
    let (directory_refresher, keep_directory_running, directory) =
        start_polling_directory_server(&configuration);

    // start listening for encrypted messages to pass
    let node_functionality = if configuration.incoming {
        // Send to dictionary server
        let _ = register_at_dictionary_server(&configuration, &public_key)?;

        // Launch incoming listener
        Some(setup_node_functionality(
            directory.clone(),
            own_main_keypair,
        ))
    } else {
        None
    };

    // Launch local webserver
    launch_rocket(directory);

    // halt created threads
    *keep_directory_running.write().unwrap() = false;
    let _ = directory_refresher.join();

    if let Some((node_handle, keep_node_running)) = node_functionality {
        *keep_node_running.write().unwrap() = false;
        node_handle
            .join()
            .map_err(|e|anyhow!("Node functionality handler panicked.\n{:?}", e))
    } else {
        Ok(())
    }
}
