pub mod url;

use self::url::Url;
use crate::{directory::Directory, node::start_request};
use rocket::{Response, State, http::Status};
use std::{io::Cursor, sync::{Arc, RwLock}};
use crate::prelude::*;
use rocket::http::hyper::mime::{Mime, TopLevel, SubLevel};
use regex::Regex;

// NOTE: do not use this in production! It's a half-assed solution to the required setup that uses 
// localhost. In reality one would use localhost as a system wide proxy. Or better even, not use a 
// local web server.
fn try_replace_links(body: &[u8], content_type: &str, url:&Url)-> Result<Vec<u8>, anyhow::Error> {
    // naive regex for detecting a url with a domain, I know
    let with_origin_regex: Regex = Regex::new(r#"(?P<prop>src|href)="(?P<domainpath>.*(\w+\.\w+)/.*)""#).unwrap();
    let without_origin_regex: Regex = Regex::new(r#"(?P<prop>src|href)="(?P<path>/.*)""#).unwrap();
    let client_prefix: String = format!("http://localhost:{}/getResource?URL=",CLIENT_PORT);

    match content_type.parse::<Mime>() {
        Ok(mime_type) => {
            match mime_type {
                Mime(TopLevel::Text, SubLevel::Html, _)=>{
                    let body = String::from_utf8_lossy(body).into_owned();

                    let body = with_origin_regex
                        .replace_all(
                            &body,
                            format!("$prop=\"{}$domainpath\"",client_prefix).as_str()
                        );
                    let body = without_origin_regex
                        .replace_all(
                            &body,
                            format!("$prop=\"{}{}$path\"",client_prefix, url.host_str().unwrap_or_default()).as_str()
                        );

                    Ok(Vec::from(body.as_bytes()))
                }
                _ => Ok(Vec::from(body)),
            }
        },
        Err(_)=> {
            bail!("Content type {}, returned by response, was not parse-able", content_type);
        }
    }
}

/// Functions used to handle the client aspect of a node.
/// I.e.: allowing users to interact with the network.
#[get("/getResource?<URL>")]
#[allow(non_snake_case)]
pub fn request_url(URL: Url, directory: State<Arc<RwLock<Directory>>>) -> Result<Response, Status> {
    println!("Requesting {}", URL.to_string());
    start_request(&URL, directory.clone())
        .and_then(|(mut body, content_type_header)|{
            let mut response = Response::build();
            
            if let Some(content_type) = content_type_header {
                body = try_replace_links(body.as_slice(), &content_type, &URL)?;
                response.raw_header(CONTENT_TYPE_HEADER, content_type);
            }
            
            response.sized_body(Cursor::new(body));
            Ok(response.finalize())
        }).map_err(|e|{
            eprintln!("e = {:#?}", e);
            Status::InternalServerError
        })
}
