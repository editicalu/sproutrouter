use std::convert::TryFrom;

use anyhow::Context;
use rocket::request::FromFormValue;
use serde::de::{self, Visitor};

#[derive(Deref, Clone, Debug)]
pub struct Url(reqwest::Url);

impl<'v> FromFormValue<'v> for Url {
    type Error = anyhow::Error;

    fn from_form_value(form_value: &'v rocket::http::RawStr) -> Result<Self, Self::Error> {
        TryFrom::<&str>::try_from(form_value)
    }
}

impl TryFrom<&str> for Url {
    type Error = anyhow::Error;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        reqwest::Url::parse(value)
            .map(Url)
            .or_else(|e| match e {
                url::ParseError::RelativeUrlWithoutBase => {
                    reqwest::Url::parse(&format!("http://{}", value))
                        .map(Url)
                }
                _ => Err(e),
            })
            .with_context(|| format!("Failed to parse {} as URL", value))
    }
}

impl serde::Serialize for Url {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(self.as_str())
    }
}
impl<'de> serde::Deserialize<'de> for Url {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_str(UrlDataVisitor)
    }
}

struct UrlDataVisitor;
impl<'de> Visitor<'de> for UrlDataVisitor {
    type Value = Url;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        formatter.write_str("A valid Url")
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        TryFrom::<&str>::try_from(v).map_err(de::Error::custom)
    }

    fn visit_bytes<E>(self, v: &[u8]) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        std::str::from_utf8(v)
            .map_err(|e| anyhow!("While parsing bytes as url string.\n{}", e))
            .and_then(|s| TryFrom::<&str>::try_from(s))
            .map_err(de::Error::custom)
    }
}
