use std::convert::TryFrom;

use anyhow::Context;
use sproutrouter_lib::crypto::{KeyPair, PublicKey};

use crate::directory::SafeNode;

#[derive(Serialize, Deserialize, Deref, DerefMut, Debug)]
pub struct ReversePathPubkeys(Vec<PublicKey>);

impl ReversePathPubkeys {
    pub fn new(keys: Vec<PublicKey>) -> Self {
        Self(keys)
    }

    pub fn try_encrypt(&self, destination_node: &SafeNode) -> Result<Vec<u8>, anyhow::Error> {
        destination_node
            .pub_key()
            .encrypt(
                &rmp_serde::encode::to_vec(&self)
                    .context("Failed to encode ReversePathPubkeys as MessagePack.")?[..],
            )
            .context("Failed to encrypt data.")
    }
}

impl TryFrom<(&KeyPair, &Vec<u8>)> for ReversePathPubkeys {
    type Error = anyhow::Error;

    fn try_from((priv_key, data): (&KeyPair, &Vec<u8>)) -> Result<Self, Self::Error> {
        rmp_serde::from_read_ref(
            &priv_key
                .decrypt(&data[..])
                .context("Failed to decrypt data with given public key.")?,
        )
        .context("Failed to decode assumed MessagePack data as ReversePathPubkeys.")
    }
}
