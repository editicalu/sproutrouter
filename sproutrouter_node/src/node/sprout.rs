use std::{convert::TryFrom, net::IpAddr};

use anyhow::Context;
use reqwest::header::HeaderValue;
use sproutrouter_lib::crypto::KeyPair;

use crate::{client::url::Url, directory::SafeNode};

use super::reverse_path_pubkeys::ReversePathPubkeys;

/// A sprout is a multi-layer encrypted message being passed between nodes.
#[derive(Serialize, Deserialize, Debug)]
pub enum SproutContent {
    Request(Url),
    Response(Vec<u8>, Option<String>),
    Data(Vec<u8>),
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Sprout {
    next_node: Option<Vec<IpAddr>>, // a node can advertise multiple IPs
    message: SproutContent,
}

impl Sprout {
    /// build the request message. Starts with the innermost layer which contains the request
    /// and encrypts it with the last node's key. Then, more and more layers, each individually
    /// encrypted, are wrapped around the request.
    pub fn try_grow_new(url: &Url, path: &[SafeNode]) -> Result<Vec<u8>, anyhow::Error> {
        // encrypted, to be sent message
        let mut encrypted_sprout = Err(anyhow!("Uninitialized sprout"));
        // "next" as in the next in the forward chain direction
        let mut next_node_ips = None;

        for node in path.iter().rev() {
            let sprout_content = if next_node_ips.is_some() {
                SproutContent::Data(encrypted_sprout?)
            } else {
                SproutContent::Request(url.clone())
            };

            let sprout = Sprout {
                next_node: next_node_ips,
                message: sprout_content,
            };

            encrypted_sprout = node
                .pub_key()
                .encrypt(
                    &rmp_serde::encode::to_vec(&sprout)
                        .context("Failed to encode Sprout as MessagePack.")?[..],
                )
                .context("Failed to encrypt data.");

            next_node_ips = Some(node.ip().clone());
        }

        encrypted_sprout
    }

    /// build the response message. Similar to `try_grow_new`, it starts with the innermost layer.
    /// But this time the innermost is meant for the original requester. Then, most layers are
    /// added. The keys used to encrypt each layer are the ones that were added to a list and
    /// passed alongside the original sprout message.
    pub fn try_grow_response(
        response_data: &[u8],
        content_type_header: &Option<HeaderValue>,
        rev_path_pubkeys: &ReversePathPubkeys,
    ) -> Result<Vec<u8>, anyhow::Error> {
        // encrypted, to be sent message
        let mut encrypted_sprout = Err(anyhow!("Uninitialized sprout"));
        let mut first_node = true;

        for key_of_node in rev_path_pubkeys.iter() {
            let sprout_content = if first_node {
                first_node = false;
                SproutContent::Response(
                    Vec::from(response_data),
                    content_type_header
                        .clone()
                        .map(|header| String::from_utf8_lossy(header.as_ref()).into_owned())
                )
            } else {
                SproutContent::Data(encrypted_sprout?)
            };

            let sprout = Sprout {
                next_node: None,
                message: sprout_content,
            };

            encrypted_sprout = key_of_node
                .encrypt(
                    &rmp_serde::encode::to_vec(&sprout)
                        .context("Failed to encode Sprout as MessagePack.")?[..],
                )
                .context("Failed to encrypt data.");
        }

        encrypted_sprout
    }

    pub fn next_node(&self) -> &Option<Vec<IpAddr>> {
        &self.next_node
    }
    pub fn message(&self) -> &SproutContent {
        &self.message
    }
}

impl TryFrom<(&KeyPair, &Vec<u8>)> for Sprout {
    type Error = anyhow::Error;

    fn try_from((priv_key, data): (&KeyPair, &Vec<u8>)) -> Result<Self, Self::Error> {
        rmp_serde::from_read_ref(
            &priv_key
                .decrypt(&data[..])
                .context("Failed to decrypt data with given private key.")?,
        )
        .context("Failed to decode assumed MessagePack data as Sprout.")
    }
}
