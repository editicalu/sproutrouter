use anyhow::Context;
use byteorder::LittleEndian;
use tokio::io::{AsyncReadExt, AsyncWriteExt};

/// TcpStream wrapper to enable easy read/write
#[derive(Deref, DerefMut)]
pub struct TcpStream(tokio::net::TcpStream);

impl TcpStream {
    pub fn new(tokio_stream: tokio::net::TcpStream) -> TcpStream {
        Self(tokio_stream)
    }

    /// Prepend a u64 to the actual data to tell the receiver the amount of
    /// bytes which will be sent.
    pub async fn write(&mut self, data: &[u8]) -> Result<(), anyhow::Error> {
        let mut to_be_sent = vec![];
        byteorder::WriteBytesExt::write_u64::<LittleEndian>(&mut to_be_sent, data.len() as u64)
            .with_context(|| format!("Failed to write {} to buffer as LE bytes.", data.len()))?;

        to_be_sent.extend_from_slice(data);
        self.0
            .write_all(to_be_sent.as_slice())
            .await
            .context("Failed to send all data over TcpStream.")
    }

    /// First reads a u64 which indicates the amount of bytes which will be sent to us.
    /// Then read this data to a buffer.
    pub async fn read(&mut self) -> Result<Vec<u8>, anyhow::Error> {
        let mut amount_bytes: [u8; 8] = [0; 8]; // u64 is 8 bytes
        self.0
            .read_exact(&mut amount_bytes)
            .await
            .context("Failed to read amount of bytes which will be sent.")?;
            
        let amount_bytes = byteorder::ReadBytesExt::read_u64::<LittleEndian>(&mut &amount_bytes[..])
            .context("Failed to convert data to `amount_bytes` u64 number.")?
            as usize;

        let mut actual_data = vec![0;amount_bytes];
        self
            .0
            .read_exact(&mut actual_data)
            .await
            .with_context(|| format!("Failed to read {} bytes of actual data.", amount_bytes))?;

        Ok(actual_data)
    }
}

impl From<tokio::net::TcpStream> for TcpStream {
    fn from(stream: tokio::net::TcpStream) -> Self {
        Self::new(stream)
    }
}
