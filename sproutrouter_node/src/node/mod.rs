mod reverse_path_pubkeys;
mod sprout;
mod tcp_stream;

use std::{ops::Deref, convert::TryFrom, net::{IpAddr, SocketAddr}, sync::{Arc, RwLock}};
use anyhow::Context;
use sprout::SproutContent;
use sproutrouter_lib::crypto::KeyPair;
use tokio::{
    net::{TcpListener, TcpStream},
};
use crate::{client::url::Url, directory::Directory};
use self::{reverse_path_pubkeys::ReversePathPubkeys, sprout::Sprout};
use crate::prelude::*;

/// Functions used to process sprouts.
/// I.e.: the handling of node functionality such as (un)wrapping and passing on messages.
const NODE_FUNCTIONALITY_PORT: u16 = 8040;
const LOCALHOST: [u8; 4] = [0, 0, 0, 0];

const MAX_CONNECTION_ATTEMPTS: u8 = 3;
const RANDOM_PATH_LEN: usize = 4;

async fn request_from_next_node(
    next_node_ips: &[IpAddr],
    encrypted_message: &[u8],
    encrypted_reverse_path_pubkeys: &[u8],
) -> Result<Vec<u8>, anyhow::Error> {
    for next_node_ip in next_node_ips.iter() {
        let mut times_connection_failed: u8 = 0;

        while times_connection_failed <= MAX_CONNECTION_ATTEMPTS {
            let connection_setup =
                TcpStream::connect(SocketAddr::new(*next_node_ip, NODE_FUNCTIONALITY_PORT))
                    .await
                    .map(Into::<tcp_stream::TcpStream>::into);

            if let Err(e) = connection_setup {
                eprintln!("e = {:#?}", e);
                times_connection_failed += 1;
                continue;
            }
            // safe to unwrap as we just checked
            let mut connection_setup = connection_setup.unwrap();

            // handle passing forward of message
            {
                let sprout_write_result = connection_setup
                    .write(encrypted_message)
                    .await;

                if let Err(e) = sprout_write_result {
                    eprintln!("e = {:#?}", e);
                    times_connection_failed += 1;
                    continue;
                }

                let rev_keys_write_result = connection_setup
                    .write(encrypted_reverse_path_pubkeys)
                    .await;

                if let Err(e) = rev_keys_write_result {
                    eprintln!("e = {:#?}", e);
                    times_connection_failed += 1;
                    continue;
                }
            }

            // handle passing backward the reply to message
            {
                let read_result = connection_setup.read().await;

                if let Err(e) = read_result {
                    eprintln!("e = {:#?}", e);
                    times_connection_failed += 1;
                    continue;
                }

                return read_result;
            }
        }
    }

    bail!(
        "Too many failed connection attempts! These IPs were attempted: {:?}",
        next_node_ips
    )
}

async fn handle_incoming_request(
    mut tcp_stream: tcp_stream::TcpStream,
    peer_addr: &IpAddr,
    own_main_keypair: KeyPair,
    directory: Arc<RwLock<Directory>>,
) -> Result<(), anyhow::Error> {
    let decrypted_incoming_message = {
        let buffer = tcp_stream
            .read()
            .await
            .map_err(|e| anyhow!("Failed to read Sprout from TCP stream.\n{}", e.to_string()))?;
        Sprout::try_from((&own_main_keypair, &buffer))?
    };

    let mut decrypted_rev_path_pubkeys = {
       let buffer= tcp_stream.read().await.map_err(|e| {
            anyhow!(
                "Failed to read ReversePathPubkeys from TCP stream.\n{}",
                e.to_string()
            )
        })?;
        ReversePathPubkeys::try_from((&own_main_keypair, &buffer))?
    };

    match decrypted_incoming_message.next_node() {
        // we gotta pass the message on if the decrypted message's `next_node` field is populated
        Some(next_node_ips) => {
            println!("Forward: from {:?}", peer_addr);
            println!("      through {:?}", tcp_stream.local_addr());
            println!("      to {:?}", next_node_ips);    

            let own_reverse_keypair =
            KeyPair::gen().context("Failed to generate keypair to use on reverse path.")?;

            // add own key to list of reverse keys
            let encrypted_rev_path_pubkeys = {
                decrypted_rev_path_pubkeys.push(own_reverse_keypair.public_key());

                let directory = directory.read().unwrap();
                let next_node=next_node_ips.iter().find_map(|ip| directory.find_node(ip)).ok_or_else(|| anyhow!("No known node with IPs: {:?}", next_node_ips))?;
                decrypted_rev_path_pubkeys.try_encrypt(next_node)?
            };

            match decrypted_incoming_message.message() {
                SproutContent::Data(data) => {
                    let encrypted_response = 
                        request_from_next_node(next_node_ips, data, &encrypted_rev_path_pubkeys)
                        .await
                        .with_context(|| {
                            format!(
                                "Something went wrong while passing a request\nfrom {:?}\nthrough {:?}\nto {:?}",
                                peer_addr,
                                tcp_stream.local_addr(),
                                next_node_ips
                            )
                        })?;

                    let decrypted_response = Sprout::try_from((&own_reverse_keypair, &encrypted_response))?;

                    match decrypted_response.message(){
                        SproutContent::Data(data) => return tcp_stream.write(data).await.context("Failed to send back response to requester."),
                        _=> bail!("A non-blob SproutContent was detected while passing responses back on an intermediate node.")
                    }
                },
                _=> bail!(
                    "A next node being present indicates that this message should be passed on, but the message is not a blob."
                )
            }
        }
        None => {
            match decrypted_incoming_message.message() {
                SproutContent::Request(url) => {
                    let response = 
                        reqwest::get(url.deref().clone())
                            .await
                            .context(format!("Failed to get {}", url.as_str()))?;

                    let content_type_header = response.headers().get(CONTENT_TYPE_HEADER).cloned();
                    let response_body = response.bytes().await.context("Couldn't get response body")?;

                    let encrypted_response = 
                        Sprout::try_grow_response(
                            &response_body,
                            &content_type_header,
                            &decrypted_rev_path_pubkeys
                        ).context("Failed to build encrypted response message on exit-node.")?;
                    
                    tcp_stream.write(encrypted_response.as_slice())
                        .await
                        .context("Failed to send back response from exit-node.")
                }
                SproutContent::Response(_,_) => bail!("A node contacting us should never send a response. Responses are sent over already opened connections."),
                SproutContent::Data(_) => bail!("The message is supposedly a blob, but no next node to send it to was indicated.")
            }
        }
    }
}

#[tokio::main]
pub async fn start_listening_for_requests_as_node(
    keep_running: Arc<RwLock<bool>>,
    directory: Arc<RwLock<Directory>>,
    own_main_keypair: KeyPair,
) {
    let mut failed_connections: u8 = 0;

    // start listening for tcp connections and handle them concurrently
    let mut listener =
        TcpListener::bind(SocketAddr::from((LOCALHOST, NODE_FUNCTIONALITY_PORT)))
            .await
            .expect("Could not create TCP listener.");

    while *keep_running.read().unwrap() {
        let connection_attempt = listener.accept().await;

        match connection_attempt {
            Ok((socket, peer_addr)) => {
                let own_main_keypair_copy = own_main_keypair.clone();
                let directory_copy = directory.clone();
                let keep_running_write_copy = keep_running.clone();
                let _ = tokio::spawn(async move {
                    let tcp_handling_result = handle_incoming_request(
                        socket.into(),
                        &peer_addr.ip(),
                        own_main_keypair_copy,
                        directory_copy,
                    )
                    .await;

                    if let Err(e) = tcp_handling_result {
                        eprintln!("e = {:#?}", e);
                        failed_connections += 1;

                        if failed_connections > MAX_CONNECTION_ATTEMPTS {
                            *keep_running_write_copy.write().unwrap() = false;
                        }
                    }
                });

            }
            Err(e) => {
                eprintln!("e = {:#?}", e);
                failed_connections += 1;

                if failed_connections > MAX_CONNECTION_ATTEMPTS {
                    *keep_running.write().unwrap() = false;
                }
            }
        }
    }
}

#[tokio::main]
pub async fn start_request(
    url: &Url,
    directory: Arc<RwLock<Directory>>,
) -> Result<(Vec<u8>, Option<String>), anyhow::Error> {
    let path = {
        // sub-scope to drop read lock asap
        directory
            .read()
            .unwrap()
            .generate_random_path(RANDOM_PATH_LEN)
    };
    println!("Following path will be followed:");
    for ips in path.iter().map(|x| x.ip()){
        println!("- {:?}", ips);
    }

    if let Some(next_node) = path.get(0) {
        let encrypted_message = Sprout::try_grow_new(&url, &path)
            .context("Failed to build encrypted layers for request.")?;

        let own_reverse_keypair =
            KeyPair::gen().context("Failed to generate keypair to use on reverse path.")?;
        let encrypted_rev_pubkeys = ReversePathPubkeys::new(vec![own_reverse_keypair.public_key()])
            .try_encrypt(next_node)?;

        let encrypted_response =
            request_from_next_node(next_node.ip(), &encrypted_message, &encrypted_rev_pubkeys)
                .await
                .context("Something went wrong during the request.")?;
        let decrypted_response = Sprout::try_from((&own_reverse_keypair, &encrypted_response))?;

        match decrypted_response.message(){
            SproutContent::Response(response, content_type_header) => Ok((response.clone(), content_type_header.clone())),
            _ => bail!("Response arrived at requesting client. However, it does not contain a SproutContent::Response type.")
        }
    } else {
        bail!("No path to follow. Did path generation fail?")
    }
}
