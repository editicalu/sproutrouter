use crate::prelude::*;

use clap::{App, Arg};

pub struct Configuration {
    /// Allow incoming connections on this node.
    pub incoming: bool,
    pub directory_server: String,
}

impl Configuration {
    pub fn from_flags() -> Result<Self> {
        let matches = App::new("SproutRouter")
            .arg(
                Arg::with_name("accept incoming")
                    .short("i")
                    .long("incoming")
                    .help("Accept incoming connections on this node.")
                    .takes_value(false),
            )
            .arg(
                Arg::with_name("directory server")
                    .short("d")
                    .long("dir")
                    .help("Specify directory server")
                    .takes_value(true),
            )
            .get_matches();

        Ok(Self {
            incoming: matches.is_present("accept incoming"),
            directory_server: if let Some(val) = matches.value_of("directory server") {
                val.to_string()
            } else {
                anyhow::bail!("No directory server given")
            },
        })
    }
}
