pub use anyhow::Context;
pub use anyhow::Result;
pub use sproutrouter_lib as lib;

pub const CONTENT_TYPE_HEADER: &str = "Content-Type";
pub const CLIENT_PORT: u16 = 8080;