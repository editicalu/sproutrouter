use std::sync::{Arc, RwLock};
use std::{convert::TryFrom, net::IpAddr, str::FromStr};

use crate::prelude::*;

use lib::{crypto::PublicKey, dict_node::Node};
use pnet::ipnetwork::IpNetwork;
use rand::Rng;

const DEFAULT_SECS_BETWEEN_PULLS: u64 = 5;

#[derive(Clone, Debug)]
pub struct SafeNode {
    ip: Vec<IpAddr>,
    pub_key: PublicKey,
}

impl SafeNode {
    pub fn ip(&self) -> &Vec<IpAddr> {
        &self.ip
    }

    pub fn pub_key(&self) -> &PublicKey {
        &self.pub_key
    }
}

impl TryFrom<Node> for SafeNode {
    type Error = anyhow::Error;
    fn try_from(from: Node) -> Result<SafeNode> {
        let node_ips: Vec<IpNetwork> = pnet::datalink::interfaces()
            .into_iter()
            .flat_map(|interface| interface.ips)
            .collect();

        let ips = from
            .ip
            .iter()
            .map(
                |ip_str| match pnet::ipnetwork::IpNetwork::from_str(ip_str) {
                    Ok(x) => Ok(x),
                    Err(_) => anyhow::bail!("Invalid IP {}", ip_str),
                },
            )
            .map(Result::unwrap)
            .filter(|ip| {
                node_ips
                    .iter()
                    .any(|node_ip| ip != node_ip && (*node_ip).contains(ip.ip()))
            })
            .map(|ip| ip.ip())
            .collect();

        Ok(SafeNode {
            ip: ips,
            pub_key: PublicKey::try_from(from.pub_key.as_str()).context("Invalid public key")?,
        })
    }
}

pub fn contact_directory_server(
    url: String,
    directory_content: Arc<RwLock<Directory>>,
    keep_running: Arc<RwLock<bool>>,
) {
    let url = format!("http://{}:8000", url);

    while if let Ok(guard) = keep_running.try_read() {
        *guard
    } else {
        true
    } {
        if let Ok(response) = reqwest::blocking::get(&url) {
            let mut list_len = 0;

            if let Ok(list) = response.json::<Vec<Node>>() {
                let list: Vec<SafeNode> = list
                    .into_iter()
                    .map(SafeNode::try_from)
                    .map(Result::unwrap)
                    .collect();
                list_len = list.len();

                if let Ok(mut guard) = directory_content.write() {
                    (*guard).nodes = list;
                }
            }

            if list_len > 0 {
                // Normally you'd sleep for a decent amount of time as to not overload the 
                // directory server. 
                // However, due to our docker setup, nodes may pull early on and get e.g. only 2 
                // nodes in response (and not pull again for a long time). If the requester pulls 
                // later than this intermediate node, the generated path may contain nodes that are 
                // not known to this node. This node could then not pass on the message.
                // In production this would not be a problem as we can simply wait for a longer 
                // amount of time. This node would eventually contact the directory server and get 
                // more nodes to work with, eventually stabilizing.

                std::thread::sleep(std::time::Duration::from_secs(DEFAULT_SECS_BETWEEN_PULLS));
            } else {
                // Directory server hasn't booted yet, refresh sooner
                std::thread::sleep(std::time::Duration::from_secs(DEFAULT_SECS_BETWEEN_PULLS));
            }
        } else {
            // Wait 5 seconds
            std::thread::sleep(std::time::Duration::from_secs(DEFAULT_SECS_BETWEEN_PULLS));
        }
    }
}

pub struct Directory {
    pub nodes: Vec<SafeNode>,
}

impl Directory {
    pub fn generate_random_path(&self, mut amt_hops: usize) -> Vec<SafeNode> {
        // in case that the path length cannot be satisfied, we apply a best-effort approach.
        // I.e.: if `amt_hops` is requested to be 10, but the directory server only knows about 5
        // nodes, a path of 5 will be returned.
        // A low amount of nodes (1 or 2) is certainly problematic, but if your network is *that*
        // unpopular, maybe you need to stop and rethink your decision.
        amt_hops = std::cmp::min(self.nodes.len(), amt_hops);

        let mut selected_nodes = Vec::with_capacity(amt_hops);
        while amt_hops > 0 {
            loop {
                let random_idx = rand::thread_rng().gen_range(0, self.nodes.len());
                if !selected_nodes.contains(&random_idx) {
                    selected_nodes.push(random_idx);
                    break;
                }
            }

            amt_hops -= 1;
        }

        selected_nodes
            .into_iter()
            .map(|node_idx| self.nodes[node_idx].clone())
            .collect::<Vec<SafeNode>>()
    }

    pub fn find_node(&self, node_ip: &IpAddr) -> Option<&SafeNode> {
        self.nodes.iter().find(|node| node.ip().contains(node_ip))
    }
}

impl Default for Directory {
    fn default() -> Self {
        Self { nodes: Vec::new() }
    }
}
