#[derive(Serialize, Deserialize, Clone)]
pub struct Node {
    pub ip: Vec<String>,
    pub pub_key: String,
}
