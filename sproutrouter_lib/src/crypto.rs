use crate::prelude::*;

use openssl::{
    envelope::{Open, Seal},
    pkey::{PKey, Private, Public},
    rsa::Rsa,
    symm::Cipher,
};
use serde::{
    de::{self, Visitor},
    ser,
};
use std::convert::TryFrom;
use std::convert::TryInto;

const RSA_BITS: u32 = 4096;

#[derive(Clone, Debug)]
pub struct PublicKey {
    ssl_public_key: Rsa<Public>,
}

#[derive(Serialize, Deserialize)]
struct EncryptedMessage {
    payload: Vec<u8>,
    iv: Vec<u8>,
    encrypted_keys: Vec<u8>,
}

impl PublicKey {
    /// Encrypts the given message with this public key.
    pub fn encrypt(&self, plaintext: &[u8]) -> Result<Vec<u8>> {
        let key =
            PKey::from_rsa(self.ssl_public_key.clone()).context("failed parsing public key")?;

        let cipher = Cipher::aes_256_cbc();
        let mut seal = Seal::new(cipher, &[key]).unwrap();

        let mut encrypted = vec![0; plaintext.len() + cipher.block_size()];

        let mut len = seal
            .update(plaintext, &mut encrypted)
            .context("Failed to encrypt")?;
        len += seal
            .finalize(&mut encrypted[len..])
            .context("Failed to add seal")?;
        encrypted.truncate(len);

        let iv = seal.iv().unwrap().iter().copied().collect();
        let keys = seal.encrypted_keys();
        assert_eq!(keys.len(), 1);

        let encrypted = bincode::serialize(&EncryptedMessage {
            payload: encrypted,
            iv,
            encrypted_keys: keys[0].iter().copied().collect(),
        })
        .context("Failed to serialize message")?;

        Ok(encrypted)
    }
}

impl TryFrom<&[u8]> for PublicKey {
    type Error = anyhow::Error;
    fn try_from(key_opt: &[u8]) -> Result<PublicKey> {
        let ssl_public_key = Rsa::public_key_from_pem_pkcs1(key_opt)?;
        Ok(PublicKey { ssl_public_key })
    }
}

impl TryFrom<&str> for PublicKey {
    type Error = anyhow::Error;
    fn try_from(key_str_opt: &str) -> Result<PublicKey> {
        Ok(PublicKey::try_from(key_str_opt.as_bytes())?)
    }
}

impl TryInto<String> for &PublicKey {
    type Error = anyhow::Error;
    fn try_into(self) -> Result<String> {
        Ok(String::from_utf8(self.try_into()?)?)
    }
}
impl serde::Serialize for PublicKey {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        match TryInto::<String>::try_into(self) {
            Ok(s) => serializer.serialize_str(&s),
            Err(e) => Err(ser::Error::custom(format!(
                "Failed to serialize PublicKey.\n{}",
                e
            ))),
        }
    }
}
impl<'de> serde::Deserialize<'de> for PublicKey {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_str(PublicKeyDataVisitor)
    }
}

impl TryInto<Vec<u8>> for &PublicKey {
    type Error = anyhow::Error;
    fn try_into(self) -> Result<Vec<u8>> {
        Ok(self
            .ssl_public_key
            .public_key_to_pem_pkcs1()
            .context("Failed to convert public key into bytes")?)
    }
}

struct PublicKeyDataVisitor;
impl<'de> Visitor<'de> for PublicKeyDataVisitor {
    type Value = PublicKey;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        formatter.write_str("A valid PublicKey")
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        PublicKey::try_from(v).map_err(de::Error::custom)
    }

    fn visit_bytes<E>(self, v: &[u8]) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        PublicKey::try_from(v).map_err(de::Error::custom)
    }
}

#[derive(Clone, Debug)]
pub struct KeyPair {
    private_key: Rsa<Private>,
}

impl KeyPair {
    pub fn gen() -> Result<Self> {
        // create an EcKey from the binary form of a EcPoint
        let private_key = Rsa::generate(RSA_BITS).context("Failed to generate RSA key")?;

        private_key
            .check_key()
            .context("Failed to validate keypair")?;

        Ok(Self { private_key })
    }
}
impl KeyPair {
    pub fn decrypt(&self, ciphertext: &[u8]) -> Result<Vec<u8>> {
        // Decode message
        let deser: EncryptedMessage =
            bincode::deserialize(ciphertext).context("Failed to read message")?;

        let message = deser.payload;
        let iv = deser.iv;
        let keys = deser.encrypted_keys;

        // Retrieve private key
        let pkey =
            PKey::from_rsa(self.private_key.clone()).context("failed to load private key")?;

        let cipher = openssl::symm::Cipher::aes_256_cbc();
        let mut open = Open::new(cipher, &pkey, Some(iv.as_slice()), keys.as_slice())
            .context("failed to open evp")?;

        let mut decrypted = vec![0; ciphertext.len() * 5];
        let mut msg_len = open
            .update(&message, &mut decrypted)
            .context("failed to decrypt evp")?;

        msg_len += open
            .finalize(&mut decrypted[msg_len..])
            .context("Failed to finalize decryption")?;

        decrypted.truncate(msg_len);
        Ok(decrypted)
    }
}
impl KeyPair {
    pub fn public_key(&self) -> PublicKey {
        let key = self.private_key.public_key_to_pem_pkcs1().unwrap();
        let pkey = PublicKey::try_from(key.as_slice()).unwrap();
        pkey
    }
}
