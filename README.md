# Dependencies

To run the network, the following dependencies are required:

- Docker
- Docker-compose
- [Rustup](https://rustup.rs/)
- OpenSSL

On Arch Linux and derivative distros, these can be downloaded using:
```
pacman -S docker docker-compose rustup openssl
```

# Using the application
Pre-compiled binaries are included. If you do not wish to compile the application yourself, simply run docker-compose without anything else.

## Compiling the application
First, ensure that you are on the correct version of Rust by running the following command in the root of the project:

```
rustup override set nightly-2020-11-25
```

Compile the binaries
```
cd ./sproutrouter_node
cargo build --release
cd ../

cd ./sproutrouter_directory
cargo build --release
cd ../
```

## Running the application
Run the bash script to start the docker-compose. `docker-compose` is used to handle multiple instances of nodes. To launch a network with a given (for example 10) intermediate nodes, one would run: 
```sh
./docker-compose-local.sh up --build --scale node=10
```

The `docker-compose-repo.sh` script is only usable if you have repo access, as it will download the precompiled container from our GitLab repository.
